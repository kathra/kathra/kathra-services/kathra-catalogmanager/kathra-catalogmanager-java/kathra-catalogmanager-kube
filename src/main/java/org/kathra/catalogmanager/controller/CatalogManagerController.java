/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.catalogmanager.controller;

import com.google.gson.JsonElement;
import org.kathra.catalogmanager.Config;
import org.kathra.catalogmanager.service.CatalogManagerService;
import org.kathra.platform.model.KathraCatalogResource;
import org.kathra.platform.model.KathraCatalogResourceParameter;
import org.kathra.utils.annotations.Eager;
import org.kathra.utils.serialization.GsonUtils;
import org.kathra.utils.serialization.YamlUtils;
import io.fabric8.kubernetes.api.model.ConfigMap;
import io.fabric8.kubernetes.api.model.ConfigMapList;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.apache.log4j.Logger;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.*;

@Named("CatalogManagerController")
@Singleton
@Eager
public class CatalogManagerController implements CatalogManagerService {

    Config config = new Config();
    protected static KubernetesClient client = new DefaultKubernetesClient();
    private static Logger log = Logger.getLogger(CatalogManagerController.class);

    /**
     * Find all resources in the catalog
     *
     * @return List<KathraCatalogResource>
     */
    public List<KathraCatalogResource> getAllCatalogResources() {
        ArrayList<KathraCatalogResource> serviceList = new ArrayList();

        ConfigMapList catalogEntries = client.configMaps().inNamespace(config.getCatalogNamespace()).withLabel("kind", "catalog").list();

        for (ConfigMap catalogEntry : catalogEntries.getItems()) {
            try {
                serviceList.add(getCatalogResource(catalogEntry, true));
            } catch (Exception e) {
                log.error("Error parsing : " + catalogEntry.getMetadata().getName());
            }
        }

        return serviceList;
    }

    /**
     * Find given catalogResource
     *
     * @param id The id of the catalogResource to get (required)
     * @return KathraCatalogResource
     */
    public KathraCatalogResource getCatalogResource(String id) throws Exception {
        ConfigMap catalogEntry = getConfigMap(id);
        return getCatalogResource(catalogEntry, true);
    }

    /**
     * Find given template for catalogResource
     *
     * @param id The id of the catalogResource&#39;s template that needs to be fetched. (required)
     * @return String
     */
    public String getCatalogResourceTemplate(String id) throws Exception {
        ConfigMap catalogEntry = getConfigMap(id);
        return catalogEntry.getData().get("template.yml");
    }

    private List<KathraCatalogResourceParameter> getCatalogResourceParameters(ConfigMap catalogEntry) {
        Map<String, Object> map = YamlUtils.yamlToMap(catalogEntry.getData().get("template.yml"));
        List<Map<String, Object>> parameters = (List<Map<String, Object>>) map.get("parameters");
        LinkedList<KathraCatalogResourceParameter> resourceParameters = new LinkedList();
        for (Map<String, Object> parameter : parameters) {
            JsonElement jsonElement = GsonUtils.gson.toJsonTree(parameter);
            KathraCatalogResourceParameter sp = GsonUtils.gson.fromJson(jsonElement, KathraCatalogResourceParameter.class);
            resourceParameters.add(sp);
        }
        return resourceParameters;
    }

    private KathraCatalogResource getCatalogResource(ConfigMap catalogEntry, boolean fetchParameters) {
        KathraCatalogResource resource = new KathraCatalogResource();

        resource.name(catalogEntry.getMetadata().getAnnotations().get("kathra.name"))
                .id(catalogEntry.getMetadata().getLabels().get("project"));

        resource.categories(parseCategories(catalogEntry.getMetadata().getAnnotations().get("kathra.type")))
                .licence(catalogEntry.getMetadata().getAnnotations().get("kathra.licence"))
                .teaser(catalogEntry.getMetadata().getAnnotations().get("kathra.teaser"))
                .websiteUrl(catalogEntry.getMetadata().getAnnotations().get("kathra.website"))
                .description(catalogEntry.getMetadata().getAnnotations().get("kathra.description"))
                .iconUrl(catalogEntry.getMetadata().getAnnotations().get("kathra.icon"));
        if (fetchParameters) resource.setParameters(getCatalogResourceParameters(catalogEntry));
        //addCatalogResourceDemoUrl(resource);
        return resource;
    }

    private List<String> parseCategories(String categories) {
        List list = new ArrayList();
        if (categories != null && !categories.isEmpty()) {
            if (categories.contains(",")) {
                list = Arrays.asList(categories.split(","));
            } else {
                list.add(categories);
            }
        }
        return list;
    }

    private ConfigMap getConfigMap(String serviceName) throws Exception {
        ConfigMap configMap = client.configMaps().inNamespace(config.getCatalogNamespace()).withName("catalog-" + serviceName).get();
        if (configMap == null) {
            throw new Exception("The entry " + serviceName + " doesn't exist");
        }
        return configMap;
    }
}
